/*
 * Tranquil Java Integrated Development Environment
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2021 Autumn Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package tjide.build;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.tools.JavaCompiler;
import javax.tools.Diagnostic;
import javax.tools.DiagnosticListener;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;

import tjide.build.CompileListener;
import tjide.build.CompileTask;
import tjide.project.JavaTarget;
import tjide.project.Project;
import tjide.project.Target;

/**
 * InternalJavaCompiler compiles a single Java language source file.
 */
public class InternalJavaCompiler implements CompileTask,
                                  DiagnosticListener<JavaFileObject> {

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * The Java target this is compiling.
     */
    private JavaTarget target;

    /**
     * The internal java compiler.
     */
    private static JavaCompiler internalJdk;

    /**
     * The internal java compiler task.
     */
    private JavaCompiler.CompilationTask internalJdkTask;

    /**
     * The time at which call() was called.
     */
    private long internalJdkTaskStartTime;

    /**
     * If true, the internal compile is going OK.
     */
    private boolean internalCompileOk;

    /**
     * The listeners to be notified at compile completion.
     */
    private List<CompileListener> listeners;

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Public constructor.
     *
     * @param target the Java target this is compiling
     * @param listeners the listeners being notified of the compile
     */
    public InternalJavaCompiler(final JavaTarget target,
        final List<CompileListener> listeners) {

        this.target = target;
        this.listeners = listeners;
    }

    // ------------------------------------------------------------------------
    // CompileTask ------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Cancel an in-progress compile.  Does nothing if the compile is not
     * actually running.  Note that the internal compiler cannot be canceled.
     */
    public void cancelCompile() {
        // NOP
    }

    /**
     * Determine if compile is in progress.
     *
     * @return true if the compile is in progress
     */
    public boolean isCompileRunning() {
        if (internalJdkTask != null) {
            return true;
        }
        return false;
    }

    /**
     * Get the start time of the compile task.
     *
     * @return the system time (millis) the compile task started
     */
    public long getCompileStartTime() {
        if (internalJdkTask != null) {
            return internalJdkTaskStartTime;
        }
        return -1;
    }

    /**
     * Get the target of the compile task.
     *
     * @return the target of the compile task
     */
    public Target getTarget() {
        return target;
    }

    /**
     * Compile the target.
     *
     * @param project the project metadata
     */
    public void compile(final Project project) {
        // System.err.println("compile() " + name);

        for (CompileListener listener: listeners) {
            listener.setCompileBegin();
        }

        if (internalJdk == null) {
            internalJdk = ToolProvider.getSystemJavaCompiler();
        }

        if (internalJdk == null) {
            throw new RuntimeException("ToolProvider.getSystemJavaCompiler() " +
                " returned null, aborting compile");
        }

        File sourceFile = new File(new File(project.getRootDir(),
                project.getSourceDir()), target.getName());

        final StandardJavaFileManager fileManager =
                internalJdk.getStandardFileManager(this, null, null);

        Iterable<? extends JavaFileObject> compilationUnit =
            fileManager.getJavaFileObjects(sourceFile);

        String [] opts = {
            "-g",
            "-d", project.getRootDir() + File.separator + project.getBuildDir(),
            "-Xlint:all", "-Xpkginfo:always"
        };
        List<String> options = new ArrayList<String>(Arrays.asList(opts));
        String classPath = "";
        for (String jar: project.getJars()) {
            classPath += File.pathSeparator + project.getRootDir() +
            File.separator + project.getResourcesDir() + File.separator + jar;
        }
        if (classPath.length() > 0) {
            classPath = classPath.substring(1);
            options.add("-classpath");
            options.add(classPath);
        }
        // System.err.println("options: '" + options + "'");

        for (CompileListener listener: listeners) {
            listener.setCompileTask(this);
            listener.setCompileFile(target.getName(),
                target.getBuildFilename());
            listener.setCompileLineCount(target.getSourceLineCount(project));
            listener.setCompileAvailableMemory(Runtime.getRuntime().
                freeMemory() / 1024);
        }

        internalJdkTask = internalJdk.getTask(null, fileManager, this,
            options, null, compilationUnit);
        internalJdkTaskStartTime = System.currentTimeMillis();
        internalCompileOk = true;

        Thread compileThread = new Thread(new Runnable() {
            /**
             * Run the compile task.
             */
            public void run() {
                internalJdkTask.call();

                try {
                    fileManager.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                for (CompileListener listener: listeners) {
                    if (internalCompileOk) {
                        listener.setCompileSucceeded(true);
                    } else {
                        listener.setCompileFailed(true);
                    }
                }
                internalJdkTask = null;
            }
        });

        compileThread.start();
    }

    // ------------------------------------------------------------------------
    // DiagnosticListener -----------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Invoked when a problem is found.
     *
     * @param diagnostic a diagnostic representing the problem that was found
     */
    public void report(Diagnostic<? extends JavaFileObject> diagnostic) {
        switch (diagnostic.getKind()) {
        case WARNING:
            // Fall through...
        case MANDATORY_WARNING:
            for (CompileListener listener: listeners) {
                listener.addCompileWarning(target, diagnostic.getLineNumber(),
                    diagnostic.getColumnNumber(),
                    diagnostic.getMessage(Locale.getDefault()));
            }
            break;
        case ERROR:
            for (CompileListener listener: listeners) {
                listener.addCompileError(target, diagnostic.getLineNumber(),
                    diagnostic.getColumnNumber(),
                    diagnostic.getMessage(Locale.getDefault()));
            }
            internalCompileOk = false;
            break;
        case NOTE:
        case OTHER:
            // Do nothing
            break;
        }
    }

    // ------------------------------------------------------------------------
    // InternalJavaCompiler ---------------------------------------------------
    // ------------------------------------------------------------------------

}
