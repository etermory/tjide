/*
 * Tranquil Java Integrated Development Environment
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2021 Autumn Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package tjide.ui;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ResourceBundle;

import gjexer.TApplication;
import gjexer.event.TCommandEvent;
import gjexer.event.TMenuEvent;
import static gjexer.TCommand.*;
import static gjexer.TKeypress.*;

/**
 * ProjectNotesWindow is used to edit the project notes.
 */
public class ProjectNotesWindow extends InternalEditorWindow {

    /**
     * Translated strings.
     */
    private static ResourceBundle i18n = ResourceBundle.getBundle(ProjectNotesWindow.class.getName());

    // ------------------------------------------------------------------------
    // Constants --------------------------------------------------------------
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Public constructor.
     *
     * @param parent the main application
     */
    public ProjectNotesWindow(final TApplication parent) {
        super(parent, i18n.getString("windowTitle"));

        TranquilApplication app = (TranquilApplication) getApplication();
        editField.setText(app.getProject().getNotes());
        setupAfterEditor();
    }

    // ------------------------------------------------------------------------
    // TWindow ----------------------------------------------------------------
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // InternalEditorWindow ---------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Method that subclasses can override to handle posted command events.
     *
     * @param command command event
     */
    @Override
    public void onCommand(final TCommandEvent command) {
        if (command.equals(cmSave)) {
            // We do not save to file, just update the project notes field.
            TranquilApplication app = (TranquilApplication) getApplication();
            app.getProject().setNotes(getText());
            editField.setNotDirty();
            return;
        }

        // Didn't handle it, let children get it instead
        super.onCommand(command);
    }

    /**
     * Handle posted menu events.
     *
     * @param menu menu event
     */
    @Override
    public void onMenu(final TMenuEvent menu) {

        if (menu.getId() == TranquilApplication.MENU_FILE_SAVE) {
            // We do not save to file, just update the project notes field.
            TranquilApplication app = (TranquilApplication) getApplication();
            app.getProject().setNotes(getText());
            editField.setNotDirty();
            return;
        }

        if (menu.getId() == TranquilApplication.MENU_FILE_SAVE_AS) {
            // We do the same as InternalEditorWindow, but don't keep the
            // File information around.
            try {
                String filename = fileSaveBox(".");
                if (filename != null) {
                    editField.saveToFilename(filename);
                }
            } catch (IOException e) {
                messageBox(i18n.getString("errorDialogTitle"),
                    MessageFormat.format(i18n.
                        getString("errorSavingFile"), e.getMessage()));
            }
            return;
        }

        super.onMenu(menu);
    }

}
