/*
 * Tranquil Java Integrated Development Environment
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2021 Autumn Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package tjide.ui;

import java.util.ArrayList;
import java.util.ResourceBundle;

import gjexer.TAction;
import gjexer.TComboBox;
import gjexer.TWindow;

import tjide.project.Project;
import tjide.project.RunnableTarget;
import tjide.project.Target;

/**
 * RunTargetWindow prompts for a runnable target in the project to execute.
 */
public class RunTargetWindow extends TWindow {

    /**
     * Translated strings.
     */
    private static ResourceBundle i18n = ResourceBundle.getBundle(RunTargetWindow.class.getName());

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * The runnable targets.
     */
    private ArrayList<RunnableTarget> runnableTargets;

    /**
     * The target to run.
     */
    private TComboBox target;

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Public constructor.  The input box will be centered on screen.
     *
     * @param application the application
     * @param project the Project
     */
    public RunTargetWindow(final TranquilApplication application,
        final Project project) {

        super(application, i18n.getString("windowTitle"),
            1, 1, 60, 15, CENTERED | MODAL);

        addLabel(i18n.getString("multiple1"), 1, 1, "twindow.background.modal");
        addLabel(i18n.getString("multiple2"), 1, 3, "twindow.background.modal");

        ArrayList<String> targets = new ArrayList<String>();
        runnableTargets = new ArrayList<RunnableTarget>();
        for (Target target: project.getTargets()) {
            if (target instanceof RunnableTarget) {
                if (((RunnableTarget) target).isRunnable()) {
                    runnableTargets.add((RunnableTarget) target);
                    targets.add(target.getName());
                }
            }
        }
        target = addComboBox(1, 5, getWidth() - 4, targets, 0, 5, null);

        addButton(i18n.getString("runButton"), 19, getHeight() - 4,
            new TAction() {
                public void DO() {
                    String targetName = target.getText();
                    for (RunnableTarget t: runnableTargets) {
                        if (((Target) t).getName().equals(targetName)) {
                            try {
                                t.runTarget(application, project);
                            } catch (tjide.project.NotRunnableException e) {
                                // This is a programming error.
                                throw new IllegalStateException(e);
                            }
                            break;
                        }
                    }
                    getApplication().closeWindow(RunTargetWindow.this);
                }
            });

        addButton(i18n.getString("cancelButton"), 31, getHeight() - 4,
            new TAction() {
                public void DO() {
                    getApplication().closeWindow(RunTargetWindow.this);
                }
            });

        activate(target);

    }

    // ------------------------------------------------------------------------
    // Event Handlers ---------------------------------------------------------
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // TWindow ----------------------------------------------------------------
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // RunTargetInputBox ------------------------------------------------------
    // ------------------------------------------------------------------------

}
