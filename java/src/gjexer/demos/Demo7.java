/*
 * GJexer - GPL Java Text User Interface
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2021 Autumn Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package gjexer.demos;

import java.util.ResourceBundle;

import gjexer.TApplication;
import gjexer.TPanel;
import gjexer.TText;
import gjexer.TWindow;
import gjexer.layout.BoxLayoutManager;

/**
 * This class shows off BoxLayout and TPanel.
 */
public class Demo7 {

    /**
     * Translated strings.
     */
    private static final ResourceBundle i18n = ResourceBundle.getBundle(Demo7.class.getName());

    // ------------------------------------------------------------------------
    // Demo7 ------------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Main entry point.
     *
     * @param args Command line arguments
     */
    public static void main(final String [] args) throws Exception {
        // This demo will build everything "from the outside".

        // Swing is the default backend on Windows unless explicitly
        // overridden by gjexer.Swing.
        TApplication.BackendType backendType = TApplication.BackendType.XTERM;
        if (System.getProperty("os.name").startsWith("Windows")) {
            backendType = TApplication.BackendType.SWING;
        }
        if (System.getProperty("os.name").startsWith("Mac")) {
            backendType = TApplication.BackendType.SWING;
        }
        if (System.getProperty("gjexer.Swing") != null) {
            if (System.getProperty("gjexer.Swing", "false").equals("true")) {
                backendType = TApplication.BackendType.SWING;
            } else {
                backendType = TApplication.BackendType.XTERM;
            }
        }
        TApplication app = new TApplication(backendType);
        app.addToolMenu();
        app.addFileMenu();
        TWindow window = new TWindow(app, i18n.getString("windowTitle"),
            60, 22);
        window.setLayoutManager(new BoxLayoutManager(window.getWidth() - 2,
                window.getHeight() - 2, false));

        TPanel right = window.addPanel(0, 0, 10, 10);
        TPanel left = window.addPanel(0, 0, 10, 10);
        right.setLayoutManager(new BoxLayoutManager(right.getWidth(),
                right.getHeight(), true));
        left.setLayoutManager(new BoxLayoutManager(left.getWidth(),
                left.getHeight(), true));

        left.addText("C1", 0, 0, left.getWidth(), left.getHeight());
        left.addText("C2", 0, 0, left.getWidth(), left.getHeight());
        left.addText("C3", 0, 0, left.getWidth(), left.getHeight());
        right.addText("C4", 0, 0, right.getWidth(), right.getHeight());
        right.addText("C5", 0, 0, right.getWidth(), right.getHeight());
        right.addText("C6", 0, 0, right.getWidth(), right.getHeight());

        app.run();
    }

}
